$(document).ready(function(){

  // save comment to database
  $(document).on('click', '#submit', function(e){
    e.preventDefault();
    

    var dataObj = $('.form_submit').serialize();

    $.ajax({
      url: '_create.php',
      type: 'POST',
      dataType: 'json',
      data: dataObj,
      

      success: function(response){
        //console.log(response);
        $('.message').text(response).css('color', 'red');

        setTimeout(()=>{
          $('.message').hide();
        },5000);
        //alert(response);
        console.log('success');
        $('#fullname').val('');
        $('#city').val('');
        $('#email').val('');
        $('#phonenumber').val('');
        $('#address').val('');
        $('#salary').val('');
      }
    });
  });



  // delete from database
  $(document).on('click', '#delete', function(){
    var tr = $(this).closest('tr'),
    del_id = $(this).attr('id');
    var id = $(this).data('id');   

    $.ajax({
      url: '_delete.php',
      type: 'GET',
      data: {

      'id': id,
      },
      success: function(response){
        tr.fadeOut(500  , function(){
          $(this).remove();
          });
        
      }
    });
  });

  $(document).on('click', '#update_btn', function(e){
    e.preventDefault();


   var dataObj = $('.form_update').serialize();

    $.ajax({


      url: '_update.php',
      type: 'POST',
      data: dataObj,
     
      success: function(response){

        $('.message').text(response).css('color', 'blue');
        //   console.log('o');
        // alert(response);
        $('name').val('');
        $('city').val('');
        $('email').val('');
        $('address').val('');
        $('phonenumber').val('');
        $('salary').val('');

        $('#update_btn').show();
      }
    });   
  });
  


   var rowperpage = 5;
 
        getData();

        $("#but_prev").click(function(){
            var rowid = Number($("#txt_rowid").val());
            var allcount = Number($("#txt_allcount").val());
            rowid -= rowperpage;
            if(rowid < 0){
                rowid = 0;
            }
            $("#txt_rowid").val(rowid);
            getData();
        });

        $("#but_next").click(function(){
            var rowid = Number($("#txt_rowid").val());
            var allcount = Number($("#txt_allcount").val());
            rowid += rowperpage;

            if(rowid <= allcount){
                $("#txt_rowid").val(rowid);
                getData();
            }

        });

    /* requesting data */
    function getData(){
        var rowid = $("#txt_rowid").val();
        var allcount = $("#txt_allcount").val();

        $.ajax({
            url:'pagination.php',
            type:'post',
            data:{rowid:rowid,rowperpage:rowperpage},
            dataType:'json',
            beforeSend: function(){
               $('.loader').hide();
             },
            success:function(response){
                createTablerow(response);
            }
        });

    }
    /* Create Table */
    function createTablerow(data){

        var dataLen = data.length;

        $("#emp_table tr:not(:first)").remove();

        for(var i=0; i<dataLen; i++){
            if(i == 0){
                var allcount = data[i]['allcount'];
                $("#txt_allcount").val(allcount);
            }else{
                var id = data[i]['id'];
                var fullname = data[i]['fullname'];
                var email = data[i]['email'];
                var phonenumber = data[i]['phonenumber'];
                var address = data[i]['address'];
                var city = data[i]['city'];
                var salary = data[i]['salary'];

          $("#emp_table").append("<tr id='tr_"+i+"'></tr>");
                $("#tr_"+i).append("<td align='left'>"+fullname+"</td>");
                $("#tr_"+i).append("<td align='left'>"+email+"</td>");
                $("#tr_"+i).append("<td align='left'>"+phonenumber+"</td>");
                $("#tr_"+i).append("<td align='left'>"+address+"</td>");
                $("#tr_"+i).append("<td align='left'>"+city+"</td>");
                $("#tr_"+i).append("<td align='left'>"+salary+"</td>");
                $("#tr_"+i).append("<td align='left'><button data-id='"+id+"' id='delete'>Remove</button></td>");
                $("#tr_"+i).append('<td align="left"><button><a href="editForm.php?edit='+id+'">Edit</a></button></td>');

            }
        }
    }



  $('#key').keyup(function(){
      var key = $('#key').val();
    if(key != ""){

        $.ajax({
          url: '_search.php',
          method: 'POST',
          data:{'key':key},
          dataType:'json',

          beforeSend:function(){
                $('.loader').show();
            },
          
          success: function(data){
       
          $('.loader').hide();
          $("#emp_table").remove();
          $('#table_data').html(data);
          
          // $('#table_data').css('display', 'block');
     
         }
        });
      }
});


});
