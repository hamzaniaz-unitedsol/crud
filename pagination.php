<?php
include "connection.php";

/* Getting post data */
$rowid = $_POST['rowid'];
$rowperpage = $_POST['rowperpage'];

/* Count total number of rows */
$query = "SELECT count(*) as allcount FROM employee";
$result = mysqli_query($mysqli,$query);
$fetchresult = mysqli_fetch_array($result);
$allcount = $fetchresult['allcount'];

/* Selecting rows */
$query = "SELECT * FROM employee ORDER BY id ASC LIMIT ".$rowid.",".$rowperpage;

$result = mysqli_query($mysqli,$query);

$employee_arr = array();
$employee_arr[] = array("allcount" => $allcount);

while($row = mysqli_fetch_array($result)){
    

    $id = $row['id'];
    $fullname = $row['fullname'];
 	$email = $row['email'];
 	$phonenumber= $row['phonenumber'];
 	$address = $row['address'];
 	$city = $row['city'];
    $salary = $row['salary'];


    $employee_arr[] = array("id" => $id,"fullname" => $fullname,"salary" => $salary, "email" => $email, "phonenumber" => $phonenumber, "address" => $address, "city" => $city);
   
  
}

echo json_encode($employee_arr);
