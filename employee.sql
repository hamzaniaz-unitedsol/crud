-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2021 at 07:11 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phonenumber` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `salary` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `fullname`, `email`, `phonenumber`, `address`, `city`, `salary`) VALUES
(4, 'HAMZA NIAZ', 'hamuniaz@gmail.com', '+923339797095', 'House No. 03, Street No. 02, Sardar Khalid Jan Colony', 'Peshawar', '1000'),
(5, 'HAMZA zahid', 'hamuniaz@gmail.com', '+923339797095', 'House No. 03, Street No. 02, Sardar Khalid Jan Colony', 'Peshawar', 'salary'),
(6, 'HAMZA NIAZ', 'hamuniaz@gmail.com', '+923339797095', 'House No. 03, Street No. 02, Sardar Khalid Jan Colony', 'Peshawar', 'salary'),
(7, 'HAMZA NIAZ', 'hamuniaz@gmail.com', '+923339797095', 'House No. 03, Street No. 02, Sardar Khalid Jan Colony', 'Peshawar', '10000'),
(8, 'asd', 'ad', 'add', 'addd', 'ad', 'ad'),
(9, 'ad', 'asss', 'dss', 'ds', 'dsd', 'ds'),
(10, 'x', 'cccz', 'xxz', 'zc', 'cv', 'c'),
(11, '', '', '', '', '', ''),
(12, 'HAMZA NIAZ', 'hamunia', '12', 'xcv', 'Peshawar', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
